#include "moves.h"
#include "derived_pieces.h"
#include <ctype.h>
#include<string>

//------------------constructors-------------------------
moves::moves(coords in_i_pos, coords in_f_pos, std::shared_ptr<piece> in_moved, std::shared_ptr<piece> in_taken,
    bool in_check, bool in_check_mate, char in_promoted_to):
    initial_pos{in_i_pos}, final_pos{in_f_pos}, moved_piece{in_moved}, taken_piece{in_taken},
    check{in_check}, check_mate{in_check_mate}, promoted_to{in_promoted_to}, want_allowed_moves{false},
    want_all_moves{false}, concedes{false}, request_draw{false} {};

moves::moves(moves& in_move)
{
    initial_pos = in_move.initial_pos;
    final_pos = in_move.final_pos;
    moved_piece = in_move.moved_piece;
    taken_piece = in_move.taken_piece;
    check = in_move.check;
    check_mate = in_move.check_mate;
    promoted_to = in_move.promoted_to;
    want_allowed_moves = in_move.want_allowed_moves;
    want_all_moves = in_move.want_all_moves;
    concedes = in_move.concedes;
    request_draw = in_move.request_draw;

}

moves::moves(const moves& in_move)
{
    initial_pos = in_move.initial_pos;
    final_pos = in_move.final_pos;
    moved_piece = in_move.moved_piece;
    check = in_move.check;
    check_mate = in_move.check_mate;
    promoted_to = in_move.promoted_to;   taken_piece = in_move.taken_piece;
    want_allowed_moves = in_move.want_allowed_moves;
    want_all_moves = in_move.want_all_moves;
    concedes = in_move.concedes;
    request_draw = in_move.request_draw;
}


moves::moves(moves&& in_move)
{
    initial_pos = in_move.initial_pos;
    final_pos = in_move.final_pos;
    moved_piece = in_move.moved_piece;
    check = in_move.check;
    check_mate = in_move.check_mate;
    promoted_to = in_move.promoted_to;    taken_piece = in_move.taken_piece;
    want_allowed_moves = in_move.want_allowed_moves;
    want_all_moves = in_move.want_all_moves;
    concedes = in_move.concedes;
    request_draw = in_move.request_draw;
}

//-------------------destructors---------------------
moves::~moves(){}

//-----------------operators-----------------------
moves& moves::operator=(moves& in_move)
{
    if(&in_move == this){return *this;}
    initial_pos = in_move.initial_pos;
    final_pos = in_move.final_pos;
    moved_piece = in_move.moved_piece;
    check = in_move.check;
    check_mate = in_move.check_mate;
    promoted_to = in_move.promoted_to;   taken_piece = in_move.taken_piece;
    want_allowed_moves = in_move.want_allowed_moves;
    want_all_moves = in_move.want_all_moves;
    concedes = in_move.concedes;
    request_draw = in_move.request_draw;
    return *this;
}
moves& moves::operator=(moves&& in_move)
{
    if(&in_move == this){return *this;}
    initial_pos = in_move.initial_pos;
    final_pos = in_move.final_pos;
    moved_piece = in_move.moved_piece;
    check = in_move.check;
    check_mate = in_move.check_mate;
    promoted_to = in_move.promoted_to;   taken_piece = in_move.taken_piece;
    want_allowed_moves = in_move.want_allowed_moves;
    concedes = in_move.concedes;
    request_draw = in_move.request_draw;
    return *this;
}

coords moves::get_initial_pos()const {return initial_pos;}

coords moves::get_final_pos()const {return final_pos;}

bool moves::get_want_allowed_moves()const {return want_allowed_moves;}

bool moves::get_want_all_moves()const {return want_all_moves;}

bool moves::get_concedes()const{return concedes;}

bool moves::get_request_draw() const{return request_draw;}

void moves::set_final_pos(coords in_final_pos){final_pos = in_final_pos;}


std::ostream & operator<<(std::ostream& os, moves& in_move)
{   
    coords initial_pos{in_move.initial_pos};
    coords final_pos{in_move.final_pos};
    char moved_piece_id{in_move.moved_piece->identity()};
    moved_piece_id = std::toupper(moved_piece_id);
    char taken_piece_id{in_move.taken_piece->identity()};
    char promoted_to = std::toupper(in_move.promoted_to);

    if(final_pos.is_castle()){
        switch(final_pos.other_x()){
            case 0:
                os<<"O-O-O";
                break;
            case 7:
                os<<"O-O";
                break;
            default:
                os<<"ERROR: castle is neither kingside or queen side";
                break;
        }
        return os;
    }
    if(in_move.moved_piece->identity() == 'p'){
        if(taken_piece_id !='x'||final_pos.is_en_passant()||final_pos.is_promotion())
        switch(initial_pos.x()){
            case 0:
                os<<"a";
                break;
            case 1:
                os<<"b";
                break;
            case 2:
                os<<"c";
                break;
            case 3:
                os<<"d";
                break;
            case 4:
                os<<"e";
                break;
            case 5:
                os<<"f";
                break;
            case 6:
                os<<"g";
                break;
            case 7:
                os<<"h";
                break;
            default:
                os<<"x coordinitae out of bounds";
        }
        if(taken_piece_id !='x'||final_pos.is_en_passant()){
            os<<"x";
        }
        os<<final_pos;
        if(final_pos.is_en_passant()){
            os<<" e.p.";
        }
        if(final_pos.is_promotion()){
            os<<"="<<promoted_to;
        }
    }
    else{
        os<<moved_piece_id;
        if(taken_piece_id!='x'){
        os<<"x";
        }
        os<<final_pos;

    }

    if(in_move.check_mate){
        os<<"#";
    }
    if(in_move.check){
        os<<"+";
    }
    return os;
}

std::istream & operator>>(std::istream& is, moves& in_move)
{//take input style as XN-YM
    std::string input;
    bool valid_move{false};
    char raw_x_i,raw_y_i,raw_x_f,raw_y_f;
    int x_i{0},y_i{0},x_f{0},y_f{0};
    is >> input;

    while(valid_move == false){
        if(input == "concede"){
            in_move.initial_pos = coords(-1,-1);
            in_move.final_pos = coords(-1,-1);
            in_move.moved_piece = std::make_shared<nopiece>(nopiece(coords(0,0))); 
            in_move.taken_piece = std::make_shared<nopiece>(nopiece(coords(0,0)));
            in_move.want_allowed_moves = false;
            in_move.want_all_moves = false;
            in_move.request_draw = false;
            in_move.concedes = true;
            valid_move = true; 
            break;
        }
        else if(input == "draw"){
            in_move.initial_pos = coords(-1,-1);
            in_move.final_pos = coords(-1,-1);
            in_move.moved_piece = std::make_shared<nopiece>(nopiece(coords(0,0))); 
            in_move.taken_piece = std::make_shared<nopiece>(nopiece(coords(0,0)));
            in_move.want_allowed_moves = false;
            in_move.want_all_moves = false;
            in_move.concedes = false;
            in_move.request_draw = true;
            valid_move = true; 
            break;
        }
        else if (input == "all"){
            in_move.initial_pos = coords(-1,-1);
            in_move.final_pos = coords(-1,-1);
            in_move.moved_piece = std::make_shared<nopiece>(nopiece(coords(0,0))); 
            in_move.taken_piece = std::make_shared<nopiece>(nopiece(coords(0,0)));
            in_move.want_allowed_moves = false;
            in_move.want_all_moves = true;
            in_move.concedes = false;
            in_move.request_draw = false;
            valid_move = true; 
            break;
        }
        else if (input.size() == 5){
            raw_x_i = input[0];
            raw_y_i = input[1];
            raw_x_f = input[3];
            raw_y_f = input[4];

            switch(std::toupper(raw_x_i)){
            case 'A': x_i = 0; break;
            case 'B': x_i = 1; break;
            case 'C': x_i = 2; break;
            case 'D': x_i = 3; break;
            case 'E': x_i = 4; break;
            case 'F': x_i = 5; break;
            case 'G': x_i = 6; break;
            case 'H': x_i = 7; break;
            default: 
                std::cout<<"You did not enter a vaild move, please enter another"<<std::endl;
                std::cin>>input;
                continue;
            }

            if(isdigit(raw_y_i)){
                y_i = raw_y_i - '0';
                if(0<y_i && 9>y_i){
                    y_i -= 1 ;
                }
                else{
                    std::cout<<"You did not enter a vaild move, please enter another"<<std::endl;
                    std::cin>>input;
                    continue;
                }
            }
            else{
                std::cout<<"You did not enter a vaild move, please enter another"<<std::endl;
                std::cin>>input;
                continue;
            }
            switch(std::toupper(raw_x_f)){
                case 'A': x_f = 0; break;
                case 'B': x_f = 1; break;
                case 'C': x_f = 2; break;
                case 'D': x_f = 3; break;
                case 'E': x_f = 4; break;
                case 'F': x_f = 5; break;
                case 'G': x_f = 6; break;
                case 'H': x_f = 7; break;
                default: 
                    std::cout<<"You did not enter a vaild move, please enter another"<<std::endl;
                    std::cin>>input;
                    continue;
            }
            if(isdigit(raw_y_f)){
                y_f = raw_y_f-'0';
                if(0<y_f && 9>y_f){
                    y_f -= 1;
                }
                else{
                    std::cout<<"You did not enter a vaild move, please enter another"<<std::endl;
                    std::cin>>input;
                    continue;
                }
            }
            else{
                std::cout<<"You did not enter a vaild move, please enter another"<<std::endl;
                std::cin>>input;
                continue;
            }    

            in_move.initial_pos = coords(x_i, y_i);
            in_move.final_pos = coords(x_f,y_f);
            in_move.moved_piece = std::make_shared<nopiece>(nopiece(coords(0,0))); 
            in_move.taken_piece = std::make_shared<nopiece>(nopiece(coords(0,0)));
            in_move.want_allowed_moves = false;
            in_move.want_all_moves = false;
            in_move.concedes = false;
            in_move.request_draw = false;
            valid_move = true;
            break;
        }
        else if(input.size()==2){
            raw_x_i = input[0];
            raw_y_i = input[1];
            switch(std::toupper(raw_x_i)){
            case 'A': x_i = 0; break;
            case 'B': x_i = 1; break;
            case 'C': x_i = 2; break;
            case 'D': x_i = 3; break;
            case 'E': x_i = 4; break;
            case 'F': x_i = 5; break;
            case 'G': x_i = 6; break;
            case 'H': x_i = 7; break;
            default: 
                std::cout<<"You did not enter a vaild move, please enter another"<<std::endl;
                std::cin>>input;
                continue;
            }
            if(isdigit(raw_y_i)){
                y_i = raw_y_i - '0';
                if(0<y_i && 9>y_i){
                    y_i -= 1 ;
                }
                else{
                    std::cout<<"You did not enter a vaild move, please enter another"<<std::endl;
                    std::cin>>input;
                    continue;
                }
            }
            else{
                std::cout<<"You did not enter a vaild move, please enter another"<<std::endl;
                std::cin>>input;
                continue;
            }
            in_move.initial_pos = coords(x_i, y_i);
            in_move.final_pos = coords(-1,-1);
            in_move.moved_piece = std::make_shared<nopiece>(nopiece(coords(0,0))); 
            in_move.taken_piece = std::make_shared<nopiece>(nopiece(coords(0,0)));
            in_move.want_allowed_moves = true;
            in_move.want_all_moves = false;
            in_move.concedes = false;
            in_move.request_draw = false;
            valid_move = true; 
            break;

        }
        else{
            std::cout<<"You did not enter a vaild move, please enter another"<<std::endl;
            std::cin>>input;
            continue;
        }
    }
    return is;
}