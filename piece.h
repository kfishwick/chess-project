// piece
//abstract base class form derived pieces
//17/05/21
#ifndef PEICE_H
#define PEICE_H


#include<iostream>
#include<vector>
#include "coords.h"
#include<memory>

class piece
{
    protected:
        char colour{'n'};//w = white, b = black, n = none(defualt - no peice)
        char ID{'x'};//k = king, q = queen, b = bishop, n = knight, r = rook, p = pawn, x = no peice
        bool in_play{true};//false when peice is taken 
        coords position{}; 
        unsigned int n_moves{true};
        std::vector<coords> raw_moves;//moves allowed without context (only translations)
        unsigned int last_turn_moved; //wrt to board counter

    public:
        piece() = default;
        piece(char, char, coords, std::vector<coords>); 
        piece(piece&);
        piece(piece&&);
        virtual ~piece();
        piece& operator=(piece&&);
        piece& operator=(piece&);

        virtual std::vector<coords> allowed_moves (std::shared_ptr<piece> c_board[8][8],int n_turns) const= 0;
        virtual bool is_in_check(std::shared_ptr<piece> c_board[8][8]) const = 0;
        char identity() const;
        char get_colour() const;
        int get_n_moves() const;
        void piece_moved(int);
        void update_position(coords in_position);
        void captured();
        int get_last_turn_moved();

};

#endif