

#include "coords.h"

coords::coords(int in_x, int in_y):  x_coord{in_x}, y_coord{in_y}, en_passant{false}, castle{false}, 
 promotion{false},other_x_coord{0}, other_y_coord{0} {};

coords::coords(int in_x, int in_y, bool in_en_passant, bool in_castle, bool in_promotion,
 int in_other_x, int in_other_y):
 x_coord{in_x}, y_coord{in_y}, en_passant{in_en_passant}, castle{in_castle}, promotion{in_promotion},
 other_x_coord{in_other_x}, other_y_coord{in_other_y} {};

coords::coords(coords& c)
{
    x_coord = c.x_coord;
    y_coord = c.y_coord;
    en_passant = c.en_passant;
    castle = c.castle;
    promotion = c.promotion;
    other_x_coord = c.other_x_coord;
    other_y_coord = c.other_y_coord;
}
coords::coords(coords&& c)
{
    x_coord = c.x_coord;
    y_coord = c.y_coord;
    en_passant = c.en_passant;
    castle = c.castle;
    promotion = c.promotion;
    other_x_coord = c.other_x_coord;
    other_y_coord = c.other_y_coord;
}

coords::coords(const coords& c)
{
    x_coord = c.x_coord;
    y_coord = c.y_coord;
    en_passant = c.en_passant;
    castle = c.castle;
    promotion = c.promotion;
    other_x_coord = c.other_x_coord;
    other_y_coord = c.other_y_coord;
}

coords& coords::operator=(coords& c)
{
    if (&c == this){return *this;}
    x_coord = c.x_coord;
    y_coord = c.y_coord;
    en_passant = c.en_passant;
    castle = c.castle;
    promotion = c.promotion;
    other_x_coord = c.other_x_coord;
    other_y_coord = c.other_y_coord;
    return *this;
}
coords& coords::operator=(coords&& c)
{
    if (&c == this){return *this;}
    x_coord = c.x_coord;
    y_coord = c.y_coord;
    en_passant = c.en_passant;
    castle = c.castle;
    promotion = c.promotion;
    other_x_coord = c.other_x_coord;
    other_y_coord = c.other_y_coord;
    return *this;
}

coords& coords::operator=(const coords& c)
{
    if (&c == this){return *this;}
    x_coord = c.x_coord;
    y_coord = c.y_coord;
    en_passant = c.en_passant;
    castle = c.castle;
    promotion = c.promotion;
    other_x_coord = c.other_x_coord;
    other_y_coord = c.other_y_coord;
    return *this;
}

coords& coords::operator=(const coords&& c)
{
    if (&c == this){return *this;}
    x_coord = c.x_coord;
    y_coord = c.y_coord;
    en_passant = c.en_passant;
    castle = c.castle;
    promotion = c.promotion;
    other_x_coord = c.other_x_coord;
    other_y_coord = c.other_y_coord;
    return *this;
}

std::ostream & operator<<(std::ostream& os, coords& in_coords)
{  
    char x_gr;
    
    switch(in_coords.x()){
        case 0: x_gr = 'a'; break;
        case 1: x_gr = 'b'; break;
        case 2: x_gr = 'c'; break;
        case 3: x_gr = 'd'; break;
        case 4: x_gr = 'e'; break;
        case 5: x_gr = 'f'; break;
        case 6: x_gr = 'g'; break; 
        case 7: x_gr = 'h'; break;    
    }
    os<<x_gr<<(in_coords.y()+1);
    return os;
}

std::ostream & operator<<(std::ostream& os, const coords& in_coords)
{  
    char x_gr;
    
    switch(in_coords.x()){
        case 0: x_gr = 'A'; break;
        case 1: x_gr = 'B'; break;
        case 3: x_gr = 'C'; break;
        case 4: x_gr = 'D'; break;
        case 5: x_gr = 'E'; break;
        case 6: x_gr = 'F'; break;
        case 7: x_gr = 'G'; break;    
    }
    os<<x_gr<<(in_coords.y()+1);
    return os;
}

coords coords::operator+(coords& in_coords) const
{
    int x_f {x_coord + in_coords.x_coord};
    int y_f {y_coord + in_coords.y_coord};
    return coords(x_f,y_f);
}
coords coords::operator-(coords& in_coords) const
{
    int x_f {x_coord - in_coords.x_coord};
    int y_f {y_coord - in_coords.y_coord};
    return coords(x_f,y_f);
}

coords coords::operator+(const coords& in_coords) const
{
    int x_f {x_coord + in_coords.x_coord};
    int y_f {y_coord + in_coords.y_coord};
    return coords(x_f,y_f);
}
coords coords::operator-(const coords& in_coords) const
{
    int x_f {x_coord - in_coords.x_coord};
    int y_f {y_coord - in_coords.y_coord};
    return coords(x_f,y_f);
}

coords coords::operator*(int factor)
{
    int x_f{x_coord*factor};
    int y_f{y_coord*factor};
    return coords(x_f, y_f);
}

const coords coords::operator*(int factor) const
{
    int x_f{x_coord*factor};
    int y_f{y_coord*factor};
    return coords(x_f, y_f);
}

bool coords::operator==(const coords& c) const
//this does not compare the hidden parameters
{
    if(x_coord == c.x_coord && y_coord == c.y_coord){
        return true;
    }
    else{
        return false;
    }
}



int coords::x() const{return x_coord;}
int coords::y() const{return y_coord;}

int coords::other_x() const{return other_x_coord;}
int coords::other_y() const{return other_y_coord;}

bool coords::is_en_passant() const{return en_passant;}

bool coords::is_castle() const{return castle;}

bool coords::is_promotion() const{return promotion;}
