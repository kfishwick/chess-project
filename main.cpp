//project - board game - chess
//simulating a chess board, for two players
//kieran fishwick, 17/05/21
#include<iostream>
#include "board.h"
#include<string>

void message_to_user(std::string output){
    std::cout<<output<<std::endl;
}

int main()
{
    bool keep_playing{true}, answer_recieved;
    while(keep_playing){
        board chess_board;
        chess_board.play();
        message_to_user("another game? (y/n)");
        answer_recieved = false;
        while(answer_recieved==false){
            std::string answer;
            std::cin>>answer;
            if (answer.length()!=1){
                message_to_user("not a valid answer, please submit another");
                continue;
            }
            switch(std::toupper(answer[0])){
                case 'Y':
                    keep_playing = true;
                    answer_recieved = true;
                    break;
                case 'N':
                    keep_playing = false;
                    answer_recieved = true;
                    break;
                default:
                    message_to_user("not a valid answer, please submit another");
                    break;
            }
        }
    }
    
    return 0;
}



