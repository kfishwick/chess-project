//derived peieces
//defines the indivdual pieces nopiece, pawn, knight, bishop, rook, queen, king
//computes the allowed moves and whether the king is in check
//17/05/21

#ifndef DERIVED_PIECES_H
#define DERIVED_PIECES_H
#include "piece.h"
#include<vector>
#include<memory>


class nopiece: public piece
{
        public:
        nopiece(coords);
        nopiece(nopiece&);
        nopiece(nopiece&&);
        ~nopiece();
        std::vector<coords> allowed_moves(std::shared_ptr<piece> c_board[8][8],int n_turns) const;
        bool is_in_check(std::shared_ptr<piece> c_board[8][8]) const;
};

class pawn: public piece 
{
    public:
        pawn(char, coords);
        pawn(pawn&);
        pawn(pawn&&);
        ~pawn();
        std::vector<coords> allowed_moves(std::shared_ptr<piece> c_board[8][8],int n_turns) const;
        bool is_in_check(std::shared_ptr<piece> c_board[8][8]) const;
};

class rook: public piece 
{
    public:
        rook(char, coords);
        rook(rook&);
        rook(rook&&);
        ~rook();
        std::vector<coords> allowed_moves(std::shared_ptr<piece> c_board[8][8],int n_turns) const;
        bool is_in_check(std::shared_ptr<piece> c_board[8][8]) const;

};

class knight: public piece
{
    public:
        knight(char ,coords);
        knight(knight&);
        knight(knight&&);
        ~knight();
        std::vector<coords> allowed_moves(std::shared_ptr<piece> c_board[8][8],int n_turns) const;
        bool is_in_check(std::shared_ptr<piece> c_board[8][8]) const;
};

class bishop: public piece 
//ID:b, moves (+/-ANY_i,+/-ANY_j) where ANY_i = ANY_j
{
    public:
        bishop(char , coords);
        bishop(bishop&);
        bishop(bishop&&);
        ~bishop();
        std::vector<coords> allowed_moves(std::shared_ptr<piece> c_board[8][8],int n_turns) const;
        bool is_in_check(std::shared_ptr<piece> c_board[8][8]) const;
};

class queen: public piece 
//ID:q, moves (+/-ANY,0),(0,+/-ANY),(+/-ANY_i,+/-ANY_j) where ANY_i = ANY_j;
{

    public:
        queen(char , coords );
        queen(queen&);
        queen(queen&&);
        ~queen();
        std::vector<coords> allowed_moves(std::shared_ptr<piece> c_board[8][8], int n_turns) const;
        bool is_in_check(std::shared_ptr<piece> c_board[8][8]) const;
};

class king: public piece 
//ID:k, moves (+/-1,0),(0,+/-1),(+/-1,+/-1)
//if taken game is over
//long and short castle, rook and king both move subject to conditions
{   
    protected:
    std::vector<coords> allowed_moves_pawn{coords(-1,1),coords(1,1)};
    std::vector<coords> allowed_moves_rook{coords(1,0),
        coords(-1,0), coords(0,1),coords(0,-1)};
    std::vector<coords> allowed_moves_knight{coords(2,1), coords(-2,1),coords(2,-1), 
        coords(-2,-1),coords(1,2),coords(-1,2),coords(1,-2),coords(-1,-2)};
    std::vector<coords> allowed_moves_bishop{coords(1,1),
        coords(1,-1), coords(-1,1),coords(-1,-1)};
    std::vector<coords> allowed_moves_queen{coords(1,1),
        coords(1,-1), coords(-1,1),coords(-1,-1), coords(1,0),coords(-1,0), coords(0,1),coords(0,-1)};
    public:
        king(char in_colour, coords in_position);
        king(king&);
        king(king&&);
        ~king();
        std::vector<coords> allowed_moves(std::shared_ptr<piece> c_board[8][8],int n_turns) const;
        bool is_in_check(std::shared_ptr<piece> c_board[8][8]) const;
};


#endif