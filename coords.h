//coords class - 
//used to define the coordinate in the program
//17/05/21
#ifndef COORDS_H
#define COORDS_H

#include<iostream>

class coords
{
    protected: 
        int x_coord{};
        int y_coord{};
        bool en_passant{false}; //below are hidden paramater used for when other pieces need to be moved
        bool castle{false};
        bool promotion{false};
        int other_x_coord{};
        int other_y_coord{};
    public:
        coords() = default;
        coords(int, int);
        coords(int, int, bool, bool, bool, int, int);
        coords(coords&);
        coords(const coords&);
        coords(coords&&);
        
        
        int x() const;
        int y() const;
        int other_x() const;
        int other_y() const;
        bool is_en_passant() const;
        bool is_castle() const;
        bool is_promotion() const;
        friend std::ostream & operator<<(std::ostream&, coords&);
        friend std::ostream & operator<<(std::ostream&,const coords&);
        coords& operator=(coords&);
        coords& operator=(coords&&);
        coords& operator=(const coords&);
        coords& operator=(const coords&&);
        coords operator+(coords&) const;
        coords operator-(coords&) const;        
        coords operator+(const coords&) const;
        coords operator-(const coords&) const;
        coords operator*(int);
        bool operator==(const coords&) const;
        const coords operator*(int) const;
        
};
#endif