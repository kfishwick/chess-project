
#include "piece.h"

piece::piece(char in_colour, char in_ident, coords in_position, std::vector<coords> in_raw_moves){
    colour=in_colour;
    in_play=true;
    ID=in_ident;
    position= in_position;
    n_moves = 0;
    for(auto& it: in_raw_moves){
        raw_moves.push_back(it);
    }
    
}
piece::piece(piece&& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    for(auto& it: p.raw_moves){
        raw_moves.push_back(it);
    }
    
}

piece::piece(piece& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    for(auto& it: p.raw_moves){
        raw_moves.push_back(it);
    }
    
}
piece::~piece(){};
piece &piece::operator=(piece&& p)
{
    if(&p == this){return *this;}
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
    return *this;
}

piece &piece::operator=(piece& p)
{
    if(&p == this){return *this;}
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
    return *this;
}

char piece::identity() const {return ID;}
char piece::get_colour() const{return colour;}
int piece::get_n_moves() const{return n_moves;}
void piece::piece_moved(int in_move_no)
{
    n_moves +=1;
    last_turn_moved = in_move_no;
}
void piece::update_position(coords in_position){
    position = in_position;
    }
void piece::captured() {in_play = false;}



int piece::get_last_turn_moved(){return last_turn_moved;}


