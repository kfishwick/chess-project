#include "derived_pieces.h"


//---------nopiece------------

nopiece::nopiece(coords in_position):
            piece{'n','x', in_position, std::vector<coords>{coords(0,0)}} {};

nopiece::nopiece(nopiece& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
}
nopiece::nopiece(nopiece&& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
}

nopiece::~nopiece() {};

std::vector<coords> nopiece::allowed_moves(std::shared_ptr<piece> c_board[8][8],int n_turns) const 
{
    std::vector<coords> empty{};
    return empty;//no piece cannot move
}

bool nopiece::is_in_check(std::shared_ptr<piece> c_board[8][8]) const
{
    std::cout<<"WARNING: is_in_check cannot be performed on a nopiece";
    return false;
}

//---------pawn------------

//ID:p, MOVES: (2or1,0) on 1st move, (1,0) on not 1st move, ONLY FORWARD   
//DIFFERENT FOR TAKING (2or1,+/-1): 1st move, (1,+/-1): not 1st move
//can change to any peice when reaches opponents side (not king)
//en passant - if first move made is (2,0), if opposing pawn moves to final loc of (1,0) move, pawn is taken

pawn::pawn(char in_colour, coords in_position):
            piece{in_colour,'p', in_position, std::vector<coords>{coords(0,1),coords(-1,1),
             coords(1,1), coords(0,2)}}{};

pawn::pawn(pawn& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
}

pawn::pawn(pawn&& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
}

pawn::~pawn(){};

std::vector<coords> pawn::allowed_moves(std::shared_ptr<piece> c_board[8][8], int n_turns) const{
    std::vector<coords> returned_moves;
    coords final_pos;
    coords move;
    int direction; 
    int eighth_rank;
    int en_passant_rank;

    if (colour == 'w'){
        direction = 1;
        eighth_rank = 7;
        en_passant_rank = 4;
    }

    else if (colour == 'b'){
        direction = -1; 
        eighth_rank = 0;
        en_passant_rank = 3;
        }

    else{
        std::cout<<"ERROR: pawn of no colour"<<std::endl;
        throw("colour error");
    }

    for(const auto& raw_it: raw_moves){

        final_pos = position + raw_it*direction;

        if (final_pos.x()>-1 && final_pos.x()<8 && final_pos.y()>-1 && final_pos.y()<8){
            //^checks move is in range
            //double first move
            if(raw_it.y() == 2 && raw_it.x()==0){

                if(c_board[final_pos.x()][final_pos.y()]->identity() == 'x'
                 && c_board[position.x()][position.y()+(direction*1)]->identity() == 'x'
                 && n_moves == 0){
                        returned_moves.push_back(final_pos);
                }
            }

            //standard move 
            else if(raw_it.y() == 1 && raw_it.x() ==0){

                if(c_board[final_pos.x()][final_pos.y()]->identity() == 'x'){

                    if (final_pos.y()==eighth_rank){//check for promotion
                        returned_moves.push_back(coords(final_pos.x(),final_pos.y(),false,false,true,0,0));
                    }

                    else{
                        returned_moves.push_back(final_pos);
                    }
                }
            }

            //standard capture
            else if(raw_it.y() == 1 && (raw_it.x() ==1||raw_it.x() == -1)){
                if(c_board[final_pos.x()][final_pos.y()]->identity() != 'x'
                 && c_board[final_pos.x()][final_pos.y()]->get_colour() != colour){

                    if (final_pos.y()==eighth_rank){//check for promotion

                        returned_moves.push_back(coords(final_pos.x(),final_pos.y(),false,false,true,0,0));
                    }

                    else{
                        returned_moves.push_back(final_pos);
                    }
                }

                if(c_board[final_pos.x()][final_pos.y()]->identity() == 'x'//en passant
                 && c_board[final_pos.x()][position.y()]->identity() == 'p'
                 && c_board[final_pos.x()][position.y()]->get_colour() != colour){
                     
                    if (c_board[final_pos.x()][position.y()]->
                     get_n_moves()==1
                     && c_board[final_pos.x()][position.y()]->
                     get_last_turn_moved() == n_turns - 1
                     && position.y() == en_passant_rank){
                        returned_moves.push_back(coords(final_pos.x(),final_pos.y(),true,false,false,final_pos.x(),position.y()));        
                                        
                    }
                }
            }
        }
    }
    return returned_moves;
}
bool pawn::is_in_check(std::shared_ptr<piece> c_board[8][8]) const
{   
        std::cout<<"WARNING: is_in_check cannot be performed on a pawn";
    return false;
}
//---------rook------------

//ID:r, MOVES:(+/-ANY,0) or (0,+/-ANY) (v,h) 0>any>8
//long and short castle- both king and rook move, under certain conditions

rook::rook(char in_colour, coords in_position):
    piece{in_colour,'r', in_position, std::vector<coords>{coords(1,0),
     coords(-1,0), coords(0,1),coords(0,-1)}} {};

rook::rook(rook& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
}

rook::rook(rook&& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
}


rook::~rook(){};

std::vector<coords> rook::allowed_moves(std::shared_ptr<piece> c_board[8][8], int n_turns)  const{
    std::vector<coords> returned_moves;
    int direction;
    coords final_pos;
    if (colour == 'w'){
        direction = 1;
    }

    else if (colour == 'b'){
        direction = -1; 
    }
        else{
        std::cout<<"ERROR: rook of no colour"<<std::endl;
        throw("colour error");
    }

    for (const auto& raw_it: raw_moves){
        for(int factor{1}; factor<8; factor++){//note unusual iteration
            final_pos = position + raw_it*(direction*factor);
            if (final_pos.x()<0 || final_pos.x()>7 || final_pos.y()<0 || final_pos.y()>7){
                break;
            }
            else if (c_board[final_pos.x()][final_pos.y()]->identity() == 'x'){
                returned_moves.push_back(final_pos);
            }
            else if (c_board[final_pos.x()][final_pos.y()]->identity() != 'x'
                && c_board[final_pos.x()][final_pos.y()]->get_colour() != colour){
                returned_moves.push_back(final_pos);
                break;
            }
            else if (c_board[final_pos.x()][final_pos.y()]->identity() != 'x'
                && c_board[final_pos.x()][final_pos.y()]->get_colour() == colour){
                break;
            }
        }
    }
    return returned_moves;
}

bool rook::is_in_check(std::shared_ptr<piece> c_board[8][8]) const
{
    std::cout<<"WARNING: is_in_check cannot be performed on a rook";
    return false;
}
//---------knight------------

//ID:n, moves (+/-2v,+/-1) or (+/-1v,+/-2) (lshape)
//CAN jump peices

knight::knight(char in_colour,coords in_position):
    piece{in_colour,'n', in_position,std::vector<coords>{coords(2,1), coords(-2,1),coords(2,-1), 
     coords(-2,-1),coords(1,2),coords(-1,2),coords(1,-2),coords(-1,-2)}}{};

knight::knight(knight& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
}

knight::knight(knight&& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
}

knight::~knight(){};

std::vector<coords> knight::allowed_moves(std::shared_ptr<piece> c_board[8][8], int n_turns)  const{
    std::vector<coords> returned_moves;
    int direction;
    coords final_pos;
    if (colour == 'w'){
        direction = 1;
    }

    else if (colour == 'b'){
        direction = -1; 
    }

    else{
        std::cout<<"ERROR: knight of no colour"<<std::endl;
        throw("colour error");
    }
    for (const auto& raw_it: raw_moves){
        final_pos = position + raw_it*direction;
        if (final_pos.x()>-1 && final_pos.x()<8 && final_pos.y()>-1 && final_pos.y()<8){
            if(c_board[final_pos.x()][final_pos.y()]->get_colour()!=colour){
                returned_moves.push_back(final_pos);
            }
        } 
    }
    return returned_moves;}

bool knight::is_in_check(std::shared_ptr<piece> c_board[8][8]) const
{   
    std::cout<<"WARNING: is_in_check cannot be performed on a knight";
    return false;
}
//---------bishop------------

//ID:b, moves (+/-ANY_i,+/-ANY_j) where ANY_i = ANY_j


bishop::bishop(char in_colour, coords in_position):
    piece{in_colour,'b', in_position,std::vector<coords>{coords(1,1),
     coords(1,-1), coords(-1,1),coords(-1,-1)}}{};

bishop::bishop(bishop& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
}

bishop::bishop(bishop&& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
}

bishop::~bishop(){};

std::vector<coords> bishop::allowed_moves(std::shared_ptr<piece> c_board[8][8], int n_turns) const{
        std::vector<coords> returned_moves;
    int direction;
    coords final_pos;
    if (colour == 'w'){
        direction = 1;
    }

    else if (colour == 'b'){
        direction = -1; 
        }
    else{
        std::cout<<"ERROR: bishop of no colour"<<std::endl;
        throw("colour error");
    }

    for (const auto& raw_it: raw_moves){
        for(int factor{1}; factor<8; factor++){//note unusual iteration
            final_pos = position + raw_it*(direction*factor);
            if (final_pos.x()<0 || final_pos.x()>7 || final_pos.y()<0 || final_pos.y()>7){
                break;
            }
            else if (c_board[final_pos.x()][final_pos.y()]->identity() == 'x'){
                returned_moves.push_back(final_pos);
            }
            else if (c_board[final_pos.x()][final_pos.y()]->identity() != 'x'
                && c_board[final_pos.x()][final_pos.y()]->get_colour() != colour){
                returned_moves.push_back(final_pos);
                break;
            }
            else if (c_board[final_pos.x()][final_pos.y()]->identity() != 'x'
                && c_board[final_pos.x()][final_pos.y()]->get_colour() == colour){
                break;
            }
        }
    }
    return returned_moves;
}

bool bishop::is_in_check(std::shared_ptr<piece> c_board[8][8]) const
{
    std::cout<<"WARNING: is_in_check cannot be performed on a bishop";
    return false;
}
//---------queen------------

//ID:q, moves (+/-ANY,0),(0,+/-ANY),(+/-ANY_i,+/-ANY_j) where ANY_i = ANY_j;

queen::queen(char in_colour, coords in_position):
    piece{in_colour,'q', in_position,std::vector<coords>{coords(1,1),
     coords(1,-1), coords(-1,1),coords(-1,-1), coords(1,0),coords(-1,0), coords(0,1),coords(0,-1)}}{};

queen::queen(queen& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
}

queen::queen(queen&& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
}
queen::~queen(){};

std::vector<coords> queen::allowed_moves(std::shared_ptr<piece> c_board[8][8], int n_turns) const{
            std::vector<coords> returned_moves;
    int direction;
    coords final_pos;
    if (colour == 'w'){
        direction = 1;
    }

    else if (colour == 'b'){
        direction = -1; 
        }
    else{
        std::cout<<"ERROR: queen of no colour"<<std::endl;
        throw("colour error");
        }

    for (const auto& raw_it: raw_moves){
        for(int factor{1}; factor<8; factor++){//note unusual iteration
            final_pos = position + raw_it*(direction*factor);
            if (final_pos.x()<0 || final_pos.x()>7 || final_pos.y()<0 || final_pos.y()>7){
                break;
            }
            else if (c_board[final_pos.x()][final_pos.y()]->identity() == 'x'){
                returned_moves.push_back(final_pos);
            }
            else if (c_board[final_pos.x()][final_pos.y()]->identity() != 'x'
                && c_board[final_pos.x()][final_pos.y()]->get_colour() != colour){
                    returned_moves.push_back(final_pos);
                break;
            }
            else if (c_board[final_pos.x()][final_pos.y()]->identity() != 'x'
                && c_board[final_pos.x()][final_pos.y()]->get_colour() == colour){
                break;
            }
        }
    }
    return returned_moves;
}

bool queen::is_in_check(std::shared_ptr<piece> c_board[8][8]) const
{
    std::cout<<"WARNING: is_in_check cannot be performed on a queen";
    return false;
}
//---------king------------

//ID:k, moves (+/-1,0),(0,+/-1),(+/-1,+/-1)
//if taken game is over
//long and short castle, rook and king both move subject to conditions

king::king(char in_colour, coords in_position):
    piece{in_colour,'k', in_position,std::vector<coords>{coords(1,1),
     coords(1,-1), coords(-1,1),coords(-1,-1), coords(1,0),coords(-1,0), coords(0,1),coords(0,-1)}}{};

king::king(king& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
}

king::king(king&& p)
{
    colour = p.colour;
    ID = p.ID;
    in_play = p.in_play;
    position = p.position;
    n_moves = p.n_moves;
    raw_moves = p.raw_moves;
}

king::~king(){};

std::vector<coords> king::allowed_moves(std::shared_ptr<piece> c_board[8][8], int n_turns) const{
    std::vector<coords> returned_moves;
    int direction;
    coords final_pos;
    coords king_initial_pos;
    coords king_side;
    coords queen_side;
    if (colour == 'w'){
     direction = 1;
        king_initial_pos = coords(4,0);
        king_side = coords(7,0);
        queen_side = coords(0,0);
    }

    else if (colour == 'b'){
        direction = -1; 
        king_initial_pos = coords(4,7);
        king_side = coords(7,7);
        queen_side = coords(0,7);
    }   
    else{
        std::cout<<"ERROR: king of no colour"<<std::endl;
        throw("colour error");
        }

    for (const auto& raw_it: raw_moves){
        final_pos = position + raw_it*direction;
        if (final_pos.x()>-1 && final_pos.x()<8 && final_pos.y()>-1 && final_pos.y()<8){
            if(c_board[final_pos.x()][final_pos.y()]->get_colour()!=colour){
                returned_moves.push_back(final_pos);
            }
        } 
    }//v : king side castling
    if(n_moves == 0 && c_board[king_side.x()][king_side.y()]->identity()=='r'
         && c_board[king_side.x()][king_side.y()]->get_colour() == colour
         && c_board[king_side.x()][king_side.y()]->get_n_moves()==0){
        if(c_board[king_side.x()-1][king_side.y()]->identity()=='x'
         && c_board[king_side.x()-2][king_side.y()]->identity()=='x'){

            returned_moves.push_back(coords(position.x()+2, position.y(),
                false, true, false, king_side.x(), king_side.y()));
        }
    }//v : queen side castling
    if(n_moves == 0 && c_board[queen_side.x()][queen_side.y()]->identity()=='r'
         && c_board[queen_side.x()][queen_side.y()]->get_colour() == colour
         && c_board[queen_side.x()][queen_side.y()]->get_n_moves()==0){
        if(c_board[queen_side.x()+1][queen_side.y()]->identity()=='x'
         && c_board[queen_side.x()+2][queen_side.y()]->identity()=='x'){

            returned_moves.push_back(coords(position.x()-2, position.y(),
                false, true, false, queen_side.x(), queen_side.y()));

        }
    }     

    return returned_moves;
}


bool king::is_in_check(std::shared_ptr<piece> c_board[8][8]) const
{//test if the king can be taking in current configuration
    int direction;
    coords final_pos;
    if (colour == 'w'){
        direction = 1;
    }

    else if (colour == 'b'){
        direction = -1; 
    }   

    for(const auto& pawn_it: allowed_moves_pawn){

        final_pos = position + pawn_it*direction;

        if (final_pos.x()>-1 && final_pos.x()<8 && final_pos.y()>-1 && final_pos.y()<8){
            //^checks move is in range        

            //standard capture

            if(c_board[final_pos.x()][final_pos.y()]->identity() == 'p'
                && c_board[final_pos.x()][final_pos.y()]->get_colour() != colour){
                return true;
            }
        
        }
    }
    for (const auto& rook_it: allowed_moves_rook){
        for(int factor{1}; factor<8; factor++){//note unusual iteration
            final_pos = position + rook_it*(direction*factor);
            if (final_pos.x()<0 || final_pos.x()>7 || final_pos.y()<0 || final_pos.y()>7){
                break;
            }
            else if (c_board[final_pos.x()][final_pos.y()]->identity() == 'r'
                && c_board[final_pos.x()][final_pos.y()]->get_colour() != colour){
                return true;
            }
            else if (c_board[final_pos.x()][final_pos.y()]->identity() != 'x'){
                break;
            }
        }
    }
    for (const auto& knight_it: allowed_moves_knight){
        final_pos = position + knight_it*direction;
        if (final_pos.x()>-1 && final_pos.x()<8 && final_pos.y()>-1 && final_pos.y()<8){
            if(c_board[final_pos.x()][final_pos.y()]->identity()=='n'
                &&c_board[final_pos.x()][final_pos.y()]->get_colour()!=colour){
                return true;
            }
        } 
    }
    for (const auto& bishop_it: allowed_moves_bishop){
        for(int factor{1}; factor<8; factor++){//note unusual iteration
            final_pos = position + bishop_it*(direction*factor);
            if (final_pos.x()<0 || final_pos.x()>7 || final_pos.y()<0 || final_pos.y()>7){
                break;
            }
            else if (c_board[final_pos.x()][final_pos.y()]->identity() == 'b'
                && c_board[final_pos.x()][final_pos.y()]->get_colour() != colour){
                return true;
            }
            else if (c_board[final_pos.x()][final_pos.y()]->identity() != 'x'){
                break;
            }
        }
    }
    for (const auto& queen_it: allowed_moves_queen){
        for(int factor{1}; factor<8; factor++){//note unusual iteration
            final_pos = position + queen_it*(direction*factor);
            if (final_pos.x()<0 || final_pos.x()>7 || final_pos.y()<0 || final_pos.y()>7){
                break;
            }
            else if (c_board[final_pos.x()][final_pos.y()]->identity() == 'q'
                && c_board[final_pos.x()][final_pos.y()]->get_colour() != colour){
                return true;
            }
            else if (c_board[final_pos.x()][final_pos.y()]->identity() != 'x'){
                break;
            }
        }
    }
    for (const auto& raw_it: raw_moves){
        final_pos = position + raw_it*direction;
        if (final_pos.x()>-1 && final_pos.x()<8 && final_pos.y()>-1 && final_pos.y()<8){
            if(c_board[final_pos.x()][final_pos.y()]->identity()=='k'
                &&c_board[final_pos.x()][final_pos.y()]->get_colour()!=colour){
                return true;
            }
        } 
    }
    return false;
}
