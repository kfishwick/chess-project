#include "board.h"
#include<string>

board::board(){//default constructor builds starting layout

    turn = 'w';
    not_turn = 'b';
    turn_name = "white";
    not_turn_name = "black";
    game_not_over = true;
    player_in_check = false;
    no_of_turns = 0;
    for(int i{0};i<w;i++){
        for(int j{0};j<h;j++){
            grid[i][j] = std::make_shared<nopiece>(std::move((coords(i,j))));//set each peice to a nopeice
        }
    }
            
    for(size_t i{0}; i<w; i++){//pawns 
        size_t j_w {1};
        size_t j_b{6};
        grid[i][j_w] = std::make_shared<pawn>(std::move(pawn('w',coords(i,j_w))));
        grid[i][j_b] = std::make_shared<pawn>(std::move(pawn('b',coords(i,j_b))));
    }
            
    for(size_t i{0};i<w; i++){
        size_t j_w{0};
        size_t j_b{7};
        if(i==0||i==7){//rook
            grid[i][j_w] = std::make_shared<rook>(std::move(rook('w',coords(i,j_w))));
            grid[i][j_b] = std::make_shared<rook>(std::move(rook('b',coords(i,j_b))));
        }
        else if(i==1||i==6){//knight
            grid[i][j_w] = std::make_shared<knight>(std::move(knight('w',coords(i,j_w))));
            grid[i][j_b] = std::make_shared<knight>(std::move(knight('b',coords(i,j_b))));
        }
        else if(i==2||i==5){//bishop
            grid[i][j_w] = std::make_shared<bishop>(std::move(bishop('w',coords(i,j_w))));
            grid[i][j_b] = std::make_shared<bishop>(std::move(bishop('b',coords(i,j_b))));
        }
        else if(i==3){//queen
            grid[i][j_w] = std::make_shared<queen>(std::move(queen('w',coords(i,j_w))));
            grid[i][j_b] = std::make_shared<queen>(std::move(queen('b',coords(i,j_b))));
                }
        else if(i==4){//king
            grid[i][j_w] = std::make_shared<king>(std::move(king('w',coords(i,j_w))));
            grid[i][j_b] = std::make_shared<king>(std::move(king('b',coords(i,j_b))));
        }
            
    }
        
}

board::~board()
{

}

std::shared_ptr<piece> board::get_piece(coords pos)
{
    return grid[pos.x()][pos.y()];
}

std::ostream & operator<<(std::ostream &os, board& in_board)
{   
    std::string w_seperator{"-"};
    std::string b_seperator{"+"};
    if(in_board.turn == 'w'){
        std::cout<<"B Taken     A   B   C   D   E   F   G   H     W Taken"<<std::endl;
        for(int j{7}; j >= 0; j--){
            std::cout<<"          |---|---|---|---|---|---|---|---|            "<<std::endl;
            if(in_board.taken_black.size()>2*(7-j)){//odd indexs here - taken pieces are always at top of board
                os<<in_board.taken_black[2*(7-j)]->get_colour()<<b_seperator<<in_board.taken_black[2*(7-j)]->identity()<<" ";
            }
            else{
                os<<"    ";
            }
            if(in_board.taken_black.size()>2*(7-j)+1){
                os<<in_board.taken_black[2*(7-j)+1]->get_colour()<<b_seperator<<in_board.taken_black[2*(7-j)+1]->identity()<<" ";
            }
            else{
                os<<"    ";
            }
            os<<j+1<<" |";
            for(size_t i{0};i<in_board.w;i++){

                if (in_board.grid[i][j]->identity()!= 'x'){

                    if(in_board.grid[i][j]->get_colour() == 'w'){
                        os<<in_board.grid[i][j]->get_colour()<<w_seperator<<in_board.grid[i][j]->identity()<<"|";
                    }
                    else if(in_board.grid[i][j]->get_colour() == 'b'){
                        os<<in_board.grid[i][j]->get_colour()<<b_seperator<<in_board.grid[i][j]->identity()<<"|";
                    }
                    
                }
                else{
                    os<<"   |";

                }
            }
            os<<" "<<j+1;
            if(in_board.taken_white.size()>2*(7-j)){
                os<<" "<<in_board.taken_white[2*(7-j)]->get_colour()<<w_seperator<<in_board.taken_white[2*(7-j)]->identity();
            }
            else{
                os<<"    ";
            }
            if(in_board.taken_white.size()>2*(7-j)+1){
                os<<" "<<in_board.taken_white[2*(7-j)+1]->get_colour()<<w_seperator<<in_board.taken_white[2*(7-j)+1]->identity();
            }
            else{
                os<<"    ";
            }
            os<<std::endl;
        }
        std::cout<<"          |---|---|---|---|---|---|---|---|            "<<std::endl;
        std::cout<<"            A   B   C   D   E   F   G   H              "<<std::endl;
        os<<std::endl;
        }
        if(in_board.turn=='b'){
        
        std::cout<<"W Taken     H   G   F   E   D   C   B   A     B Taken"<<std::endl;
        for(int j{0}; j < in_board.h; j++){
            std::cout<<"          |---|---|---|---|---|---|---|---|            "<<std::endl;
            if(in_board.taken_white.size()>2*j){
                os<<in_board.taken_white[2*j]->get_colour()<<w_seperator<<in_board.taken_white[2*j]->identity()<<" ";
            }
            else{
                os<<"    ";
            }
            if(in_board.taken_white.size()>2*j+1){
                os<<in_board.taken_white[2*j+1]->get_colour()<<w_seperator<<in_board.taken_white[2*j+1]->identity()<<" ";
            }
            else{
                os<<"    ";
            }
            os<<j+1<<" |";
            for(int i{7};i>=0;i--){

                if (in_board.grid[i][j]->identity()!= 'x'){
            
                    if(in_board.grid[i][j]->get_colour() == 'w'){
                        os<<in_board.grid[i][j]->get_colour()<<w_seperator<<in_board.grid[i][j]->identity()<<"|";
                    }
                    else if(in_board.grid[i][j]->get_colour() == 'b'){
                        os<<in_board.grid[i][j]->get_colour()<<b_seperator<<in_board.grid[i][j]->identity()<<"|";
                    }
                }
                else{
                    os<<"   |";

                }
            }
            os<<" "<<j+1;
            if(in_board.taken_black.size()>2*j){
                os<<" "<<in_board.taken_black[2*j]->get_colour()<<b_seperator<<in_board.taken_black[2*j]->identity();
            }
            else{
                os<<"    ";
            }
            if(in_board.taken_black.size()>2*j+1){
                os<<" "<<in_board.taken_black[2*j+1]->get_colour()<<b_seperator<<in_board.taken_black[2*j+1]->identity();
            }
            else{
                os<<"    ";
            }
            os<<std::endl;
        }
        std::cout<<"          |---|---|---|---|---|---|---|---|            "<<std::endl;
        std::cout<<"            H   G   F   E   D   C   B   A              "<<std::endl;
        os<<std::endl;
        
    }
    return os;
}


void board::refresh()
{//updates the locations of every piece
    for(size_t i{0};i<w;i++){
        for(size_t j{0};j<h;j++){
            grid[i][j]->update_position(coords(i,j));
        }
    }
}

void board::virtual_refresh()
{//updates the locations of every piece on the virtual grid
    for(size_t i{0};i<w;i++){
        for(size_t j{0};j<h;j++){
            virtual_grid[i][j]->update_position(coords(i,j));
        }
    }
}


bool board::check(char in_colour, bool update, coords king_pos)
{//if update is false vritual grid is not updated and will take king_pos as kings position
    bool is_in_check;
    if(update){
        for(size_t i{};i<w;i++){
            for(size_t j{};j<h;j++){
                virtual_grid[i][j] = grid[i][j];
                if(grid[i][j]->identity() == 'k' && grid[i][j]->get_colour()==in_colour){
                    king_pos = coords(i,j);
                }
            }
        }
    }
    is_in_check = virtual_grid[king_pos.x()][king_pos.y()]->is_in_check(virtual_grid);
    refresh();//to reset the pieces internal locations as changed when modififying virtual grid
    return is_in_check;
}

bool board::check(coords& initial_pos, coords& final_pos, char in_colour)
{//check whether a move puts the inputted king in check
    coords king_pos;
    for(size_t i{};i<w;i++){
        for(size_t j{};j<h;j++){
            virtual_grid[i][j] = grid[i][j];
        }
    }
    
    make_virtual_move(initial_pos, final_pos);

    for(size_t i{};i<w;i++){
        for(size_t j{};j<h;j++){
            if(virtual_grid[i][j]->identity() == 'k' && virtual_grid[i][j]->get_colour()==in_colour){
                king_pos = coords(i,j);
            }
        }
    }

    return check(in_colour, false, king_pos);
}

bool board::check_mate(char in_colour)
{//iterates over all moves, if one results in the king not being in check, returns false
//does not check current configuration as unnecessary
    std::vector<coords> pieces_in_play_pos;//stores all positions players pieces in play
    std::vector<coords> piece_allowed_moves;
    for(size_t i{};i<w;i++){
        for(size_t j{};j<h;j++){
            if(grid[i][j]->get_colour()==in_colour){
                pieces_in_play_pos.push_back(coords(i,j));
            }
        }
    }
    for(auto& initial_pos_it: pieces_in_play_pos){
        piece_allowed_moves = grid[initial_pos_it.x()][initial_pos_it.y()]->
            allowed_moves(grid,no_of_turns);
        for(auto& final_pos_it: piece_allowed_moves){
            if(check(initial_pos_it,final_pos_it,in_colour)==false){
                return false;
            }
        }
        std::cout<<std::endl;
    }
    return true;
}

bool board::request_draw()
{//requests draw 
    message_to_user(not_turn_name+", "+turn_name+" has requested a draw, would you like to accept? (y/n)");
    while(true){
        std::string answer;
        std::cin>>answer;
        if (answer.length()!=1){
            message_to_user("not a valid answer, please submit another");
            continue;
        }
        switch(std::toupper(answer[0])){
            case 'Y':
                return true;
                break;
            case 'N':
                return false;
                break;
            default:
                message_to_user("not a valid answer, please submit another");
                break;
        }

    }
}

void board::move_virtual_piece(coords pos_i, coords pos_f)
{//moves pieces on virtual board - does not remove any taken pieces to taken vectors
    if(pos_i.x()<0||pos_i.x()>w||pos_i.y()<0||pos_i.y()>h||pos_f.x()<0||pos_f.x()>w||pos_f.y()<0||pos_f.y()>h){
        std::cout<<"ERROR: index out of range"<<std::endl;
        throw("index error");
    }

    virtual_grid[pos_f.x()][pos_f.y()] = virtual_grid[pos_i.x()][pos_i.y()];
    virtual_grid[pos_f.x()][pos_f.y()]->update_position(pos_f);
    virtual_grid[pos_i.x()][pos_i.y()] = std::make_shared<nopiece>(nopiece(pos_i));
    virtual_refresh();
}

void board::make_virtual_move(coords initial_pos, coords final_pos)
{//makes correct move (dealing with exception moves) on virtual grid
    if(final_pos.is_castle()){
        switch(final_pos.other_x()){
            
            case 0://queen side
                move_virtual_piece(initial_pos, final_pos);
                move_virtual_piece(coords(final_pos.other_x(),final_pos.other_y()),
                 coords(final_pos.x()+1, final_pos.y()));
                break;
            case 7://king side
                move_virtual_piece(initial_pos, final_pos);
                move_virtual_piece(coords(final_pos.other_x(),final_pos.other_y()),
                 coords(final_pos.x()-1, final_pos.y()));
                break;
        }
    }

    else if(final_pos.is_en_passant()){
        virtual_grid[final_pos.other_x()][final_pos.other_y()] = 
            std::make_shared<nopiece>(nopiece(coords(final_pos.other_x(),final_pos.other_y())));
        move_virtual_piece(initial_pos, final_pos);
    }

    else{
        move_virtual_piece(initial_pos, final_pos);
    }

}


void board::move_piece(coords pos_i, coords pos_f)
{//moves piece on board
    if(pos_i.x()<0||pos_i.x()>w||pos_i.y()<0||pos_i.y()>h||pos_f.x()<0||pos_f.x()>w||pos_f.y()<0||pos_f.y()>h){
        std::cout<<"ERROR: index out of range"<<std::endl;
        throw("index error");
    }
    if(pos_i.x() == pos_f.x() && pos_i.y() == pos_f.y()){
        std::cout<<"ERROR: initial and final positions equal"<<std::endl;
        throw("move error");
    }
   
    grid[pos_i.x()][pos_i.y()]->piece_moved(no_of_turns);

    if(grid[pos_f.x()][pos_f.y()]->identity() == 'x'){//this case if a simple move
        grid[pos_f.x()][pos_f.y()] = grid[pos_i.x()][pos_i.y()];
        grid[pos_f.x()][pos_f.y()]->update_position(pos_f);
        grid[pos_i.x()][pos_i.y()] = std::make_shared<nopiece>(nopiece(pos_i));
    }
    else{//this is when a piece is taken

        if(grid[pos_f.x()][pos_f.y()]->get_colour()=='w'){//adds pieces to taken vectors
            grid[pos_f.x()][pos_f.y()]->captured();
            taken_white.push_back(grid[pos_f.x()][pos_f.y()]);
        }
        else if(grid[pos_f.x()][pos_f.y()]->get_colour()=='b'){
            grid[pos_f.x()][pos_f.y()]->captured();
            taken_black.push_back(grid[pos_f.x()][pos_f.y()]);
        }
        else{
            std::cout<<"ERROR: taken piece has unexpected colour"<<std::endl;
            throw("colour error");
            }

        grid[pos_f.x()][pos_f.y()] = grid[pos_i.x()][pos_i.y()];
        grid[pos_f.x()][pos_f.y()]->update_position(pos_f);
        grid[pos_i.x()][pos_i.y()] = std::make_shared<nopiece>(nopiece(pos_i));
    }
    refresh();
}

void board::make_move(coords initial_pos, coords final_pos)
{//deals with exception moves 
    if(final_pos.is_castle()){
        switch(final_pos.other_x()){
            
            case 0://queen side
                move_piece(coords(final_pos.other_x(),final_pos.other_y()),
                 coords(3, final_pos.y()));
                move_piece(initial_pos, final_pos);

                break;
            case 7://king side
                move_piece(coords(final_pos.other_x(),final_pos.other_y()),
                 coords(5, final_pos.y()));
                move_piece(initial_pos, final_pos);

                break;
        }
    }

    else if(final_pos.is_en_passant()){
        if (turn == 'w'){
            taken_black.push_back(grid[final_pos.other_x()][final_pos.other_y()]);
            grid[final_pos.other_x()][final_pos.other_y()]->captured();
            grid[final_pos.other_x()][final_pos.other_y()] = 
                std::make_shared<nopiece>(nopiece(coords(final_pos.other_x(),final_pos.other_y())));
        }
        else{
            taken_white.push_back(grid[final_pos.other_x()][final_pos.other_y()]);
            grid[final_pos.other_x()][final_pos.other_y()]->captured();
            grid[final_pos.other_x()][final_pos.other_y()] = 
                std::make_shared<nopiece>(std::move(nopiece(coords(final_pos.other_x(),final_pos.other_y()))));
        }
        move_piece(initial_pos, final_pos);
    }

    else if(final_pos.is_promotion()){
        bool selection_made{false};
        std::string selection;
        std::cout<<"you have promoted your pawn, what would you like to promote to?"<<std::endl<<
                "please enter the piece you would like to promote to in algebraic notation"<<std::endl<<
                 "ie pawn = p knight = n etc"<<std::endl;
            
        while (selection_made == false){
            std::cin>>selection;
            if (selection.size()<1){
                std::cout<<"please enter a valid piece"<<std::endl;
                continue;
            }
            else{
                switch (std::tolower(selection[0]))
                {
                case 'p':
                    std::cout<<"strange choice, but ok"<<std::endl;
                    selection_made = true;
                    break;
                
                case 'r':
                    grid[initial_pos.x()][initial_pos.y()] = std::make_shared<rook>(std::move(rook(turn,initial_pos)));
                    selection_made = true;
                    break;
                
                case 'b':
                    grid[initial_pos.x()][initial_pos.y()] = std::make_shared<bishop>(std::move(bishop(turn,initial_pos)));
                    selection_made = true;
                    break;
                
                case 'n':
                    grid[initial_pos.x()][initial_pos.y()] = std::make_shared<knight>(std::move(knight(turn,initial_pos)));
                    selection_made = true;
                    break;
                
                case 'q':
                    grid[initial_pos.x()][initial_pos.y()] = std::make_shared<queen>(std::move(queen(turn,initial_pos)));
                    selection_made = true;
                    break;
                
                default:
                    std::cout<<"please enter a valid piece"<<std::endl;
                    break;
                }
            }
        }
        move_piece(initial_pos, final_pos);
    }

    else{
        move_piece(initial_pos, final_pos);
    }

}

bool board::is_move_allowed(coords& initial_pos, coords& final_pos, std::vector<coords> allowed_moves)
{//checks whether move is allowed, sets final pos to the iterator if equal to get type of move
    bool is_move_allowed{false}, is_en_passant{false}, is_castle{false}, is_promotion{false};

    if(grid[initial_pos.x()][initial_pos.y()]->get_colour()!=turn){                        
        return false;
        }

    for(const auto& move_it: allowed_moves){
        if(move_it == final_pos){
            final_pos = move_it;//to obtain the hidden paramaters (en passant, castle, promotion, other coords)
            is_move_allowed = true;
            break;
        }
    }
    return is_move_allowed;
}


void board::output_all_moves()
{//outputs all allowed moves 
    std::vector<coords> pieces_in_play_pos;//stores all positions players pieces in play
    std::vector<coords> piece_allowed_moves;
    for(size_t i{};i<w;i++){
        for(size_t j{};j<h;j++){
            if(grid[i][j]->get_colour()==turn){
                pieces_in_play_pos.push_back(coords(i,j));
            }
        }
    }
    for(auto& initial_pos_it: pieces_in_play_pos){
        piece_allowed_moves = grid[initial_pos_it.x()][initial_pos_it.y()]->
            allowed_moves(grid,no_of_turns);
        std::cout<<initial_pos_it<<"-> ";
        for(auto& final_pos_it: piece_allowed_moves){
            if(check(initial_pos_it,final_pos_it,turn)==false){
                std::cout<<final_pos_it<<" ";
            }
        }
        std::cout<<std::endl;
    }
}
std::vector<coords> board::check_moves(coords initial_pos, std::vector<coords> in_allowed_moves)
{//checks moves for self check
    std::vector<coords> checked_moves;

    for(auto& move_it:in_allowed_moves){
                if(check(initial_pos, move_it, turn)==false){
                    checked_moves.push_back(move_it);
                }
            }
    return checked_moves;
}

void board::output_moves(coords initial_pos, std::vector<coords> allowed_moves)
{//outputs moves for single piece
    std::cout<<"Moves allowed for "<<initial_pos<<std::endl;
    for(auto& move_it: allowed_moves){
        std::cout<<move_it<<"  ";
    }
    std::cout<<std::endl;
}

void board::message_to_user(std::string error_message){
    std::cout<<error_message<<std::endl;
}

void board::end_of_game_output(bool end_by_check_mate, bool end_by_concede,bool end_by_draw, char winner)
{//outputs end of game message
    
    if(end_by_check_mate){
        std::cout<<turn_name<<" wins by check mate!"<<std::endl;
    }
    else if(end_by_concede){
        std::cout<<not_turn_name<<" wins as "<<turn_name<<" concedes!"<<std::endl;
    }
    else if(end_by_draw){
        std::cout<<"Draw!"<<std::endl;
    }
    std::cout<<"would you like a list of moves made? (y/n)"<<std::endl;
    bool answer_recieved{false};
    while(answer_recieved==false){
        std::string answer;
        std::cin>>answer;
        if (answer.length()!=1){
            message_to_user("not a valid answer, please submit another");
            continue;
        }
        switch(std::toupper(answer[0])){
            case 'Y':
                std::cout<<moves_made.size()<<" moves made"<<std::endl;
                for(auto& move_it: moves_made){
                    std::cout<<move_it<<std::endl;
                }
                if(winner=='w'){
                        std::cout<<"1-0"<<std::endl;
                }
                else if(winner=='b'){
                        std::cout<<"0-1"<<std::endl;
                }
                else{
                    std::cout<<"0-0"<<std::endl;
                }
                answer_recieved = true;
                break;
            case 'N':
                answer_recieved = true;
                break;
            default:
                message_to_user("not a valid answer, please submit another");
                break;
        }

    }
}

void start_of_game_message()
{//gives instructions at start of game
    std::cout<<"Commands:"<<std::endl<<"-Enter moves the form: A1-B2 (or a1-b2)"<<std::endl<<
        "-To get the allowed moves of any piece coordinate ie A1 (or a1)"<<std::endl<<
        "-To get all the moves you can do enter: all"<<std::endl<<
        "-To concede enter: concede"<<std::endl<<
        "-To request a draw enter: draw"<<std::endl<<std::endl;
}

void board::play()
{  //plays the game of chess
    coords initial_pos,final_pos;
    moves requested_move;
    std::vector<coords> allowed_moves;
    moves move_performed;
    bool move_selected, move_allowed_result;
    std::shared_ptr<piece> moved_piece;
    std::shared_ptr<piece> taken_piece;
    start_of_game_message();
    while(game_not_over){
        std::cout<<*this;
        move_selected = false;
        while(move_selected == false){
            if(player_in_check){
                message_to_user(turn_name+" you are in CHECK!");
            }

            message_to_user(turn_name+", please enter your move:");
            std::cin>>requested_move;
            
            
            initial_pos = requested_move.get_initial_pos();
            final_pos = requested_move.get_final_pos();

            if(requested_move.get_concedes()){
                game_not_over = false;
                end_of_game_output(false, true, false, not_turn);
                return;
            }
            else if(requested_move.get_request_draw()){
                if(request_draw()){
                    game_not_over = false;
                    end_of_game_output(false, false, true, 'n');
                    return;
                }
                else{
                    message_to_user("request denied");
                    continue;
                }
            }
            else if(requested_move.get_want_all_moves()){
                output_all_moves();
                continue;
            }
            allowed_moves = check_moves(initial_pos, grid[requested_move.get_initial_pos().x()][
                requested_move.get_initial_pos().y()]->allowed_moves(grid,no_of_turns));
            if(requested_move.get_want_allowed_moves()){
                output_moves(initial_pos, allowed_moves);
                continue;
            }
            else{    
                move_allowed_result = is_move_allowed(initial_pos, final_pos, allowed_moves);
                if(move_allowed_result==false){
                    message_to_user("the move you entered is not allowed, please enter another");
                    continue;
                }
                else{
                    move_selected = move_allowed_result;

                    break;
                }
            }
        }
        if(player_in_check){//turns off player in check once move is over
            player_in_check = false;
        }

        moved_piece = grid[initial_pos.x()][initial_pos.y()];
        taken_piece = grid[final_pos.x()][final_pos.y()];
        make_move(initial_pos, final_pos);
        
        if(check(not_turn,true, coords(0,0))){
            if(check_mate(not_turn)){
                game_not_over = false;
                move_performed = moves(initial_pos, final_pos, moved_piece,taken_piece,false,true,
                    grid[final_pos.x()][final_pos.y()]->identity());
                moves_made.push_back(move_performed);
                end_of_game_output(true,false, false, turn);
                return;
            }
            else{
                move_performed = moves(initial_pos, final_pos, moved_piece,taken_piece,true,false,
                    grid[final_pos.x()][final_pos.y()]->identity());
                moves_made.push_back(move_performed); 
                player_in_check = true;               
            }
        }
        else{
            move_performed = moves(initial_pos, final_pos, moved_piece,taken_piece,false,false,
                grid[final_pos.x()][final_pos.y()]->identity());
            moves_made.push_back(move_performed); 
        }
        std::cout<<move_performed<<std::endl;
        switch (turn)
        {
        case 'w':
            turn = 'b';
            turn_name = "black";
            not_turn = 'w';
            not_turn_name = "white";
            break;
        
        case 'b':
            turn = 'w';
            turn_name = "white";
            not_turn = 'b';
            not_turn_name = "black";
            break;
        }

        no_of_turns++;
    }
}