//moves class
//stores moves, takes moves as input from user and outputs moves in algebriac notation
//17/05/21

#ifndef MOVE_H
#define MOVE_H

#include "coords.h"
#include "piece.h"
#include "derived_pieces.h"
#include<memory>

#include<iostream>
#include<memory>

class moves
{
    private:
        coords initial_pos{coords(0,0)};
        coords final_pos{coords(0,0)};
        std::shared_ptr<piece> moved_piece{std::make_shared<nopiece>(nopiece(coords(0,0)))};
        std::shared_ptr<piece> taken_piece{std::make_shared<nopiece>(nopiece(coords(0,0)))};
        bool check;
        bool check_mate;
        char promoted_to;
        bool want_allowed_moves;
        bool want_all_moves;
        bool concedes;
        bool request_draw;
    public:
        moves() = default;
        moves(coords in_i_pos, coords in_f_pos, std::shared_ptr<piece> in_moved, std::shared_ptr<piece> in_taken,
            bool in_check, bool in_check_mate, char in_promoted_to);
        moves(moves&);
        moves(const moves&);
        moves(moves&&);
        moves& operator=(moves&);
        moves& operator=(moves&&);  
        ~moves();

        coords get_initial_pos() const;
        coords get_final_pos() const;
        void set_final_pos(coords);
        bool get_want_allowed_moves() const;
        bool get_want_all_moves() const;
        bool get_concedes() const;
        bool get_request_draw() const;

        friend std::ostream & operator<<(std::ostream&, moves&);
        friend std::istream & operator>>(std::istream&, moves&);
};

#endif
