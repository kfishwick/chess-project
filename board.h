//board class
//used to store board configuration, move pieces around the board correctly and compute the game of chess
//17/05/21
#ifndef BOARD_H
#define BOARD_H

#include "piece.h"
#include "moves.h"
#include "coords.h"
#include "derived_pieces.h"
#include<array>

class board
{
    protected:
        const size_t h{8};//height of board
        const size_t w{8};//width of board
        unsigned int no_of_turns{0};
        std::shared_ptr<piece> grid[8][8];
        std::shared_ptr<piece> virtual_grid[8][8];
        std::vector<std::shared_ptr<piece>> taken_black;//arrays to store taken pieces 
        std::vector<std::shared_ptr<piece>> taken_white; 
        std::vector<moves> moves_made;
        char turn; //which players turn it is either w or b
        char not_turn; //which players turn it isnt
        std::string turn_name;
        std::string not_turn_name;
        bool game_not_over; 
        bool player_in_check;
        
    public:
        board();
        ~board();
        friend std::ostream & operator<<(std::ostream&, board&);
        void refresh();
        void virtual_refresh();
        bool is_move_allowed(coords&,coords&, std::vector<coords>);//returns false if move is not allowed
        bool check(coords&, coords&, char);
        bool check(char,bool,coords);
        bool check_mate(char);
        bool request_draw();
        void make_move(coords, coords);
        void move_piece(coords,coords);
        void make_virtual_move(coords, coords);
        void move_virtual_piece(coords,coords);
        std::vector<coords> check_moves(coords, std::vector<coords>);
        std::shared_ptr<piece> get_piece(coords);
        std::string coord_to_gr(int, int);
        void output_all_moves();
        void output_moves(coords, std::vector<coords>);
        void start_of_game_output();
        void end_of_game_output(bool,bool,bool,char);
        void message_to_user(std::string);

        void play();
};




#endif